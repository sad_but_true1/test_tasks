package tk.sadbuttrue.shapes.shape;

/**
 * Concrete shape, that describes triangle
 * @author Eugene Aslanov
 */
public class Triangle implements Shape {
    private final double a;
    private final double b;
    private final double c;

    /**
     * Constructs triangle from sides length
     * @param a – the first side
     * @param b – the second side
     * @param c – the third side
     */
    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Calculates area of triangle
     * @return double value of area
     */
    public double getArea() {
        double s = getPerimeter() / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    /**
     * Calculates perimeter of triangle
     * @return double value of perimeter
     */
    public double getPerimeter() {
        return a + b + c;
    }

    /**
     * Getter for side a
     * @return double value of side length
     */
    public double getA() {
        return a;
    }

    /**
     * Getter for side b
     * @return double value of side length
     */
    public double getB() {
        return b;
    }

    /**
     * Getter for side c
     * @return double value of side length
     */
    public double getC() {
        return c;
    }
}
