package tk.sadbuttrue.shapes.shape;

/**
 * Concrete shape, that describes rectangle
 *
 * @author Eugene Aslanov
 */
public class Rectangle implements Shape {
    private final double width;
    private final double height;

    /**
     * Constructs rectangle from it's width and height
     *
     * @param width  – width of rectangle
     * @param height – height of rectangle
     */
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Calculates area of rectangle
     * @return double value of area
     */
    public double getArea() {
        return width * height;
    }

    /**
     * Calculates perimeter of rectangle
     * @return double value of perimeter
     */
    public double getPerimeter() {
        return 2 * (width + height);
    }

    /**
     * Getter for width
     *
     * @return double value of width length
     */
    public double getWidth() {
        return width;
    }

    /**
     * Getter for height
     *
     * @return double value of height length
     */
    public double getHeight() {
        return height;
    }
}
