package tk.sadbuttrue.shapes.shape;

/**
 * Concrete shape, that describes circle
 * @author Eugene Aslanov
 */
public class Circle implements Shape {
    private final double radius;

    /**
     * Constructs circle from it's radius
     * @param radius – radius of circle
     */
    public Circle(double radius) {
        this.radius = radius;
    }

    /**
     * Calculates area of circle
     * @return double value of area
     */
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    /**
     * Calculates perimeter of circle
     * @return double value of perimeter
     */
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    /**
     * Getter for radius
     * @return double value of radius
     */
    public double getRadius() {
        return radius;
    }
}
