package tk.sadbuttrue.shapes.shape;

/**
 * Interface, that describes some shape with area and perimeter
 * @author Eugene Aslanov
 */
public interface Shape {
    /**
     * Calculates area of shape
     * @return double value of area
     */
    double getArea();
    /**
     * Calculates perimeter of shape
     * @return double value of perimeter
     */
    double getPerimeter();
}
