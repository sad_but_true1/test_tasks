package tk.sadbuttrue.shapes.shape;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Eugene Aslanov
 */
public class ShapeTest {
    private static Shape triangle;
    private static double triangleArea;
    private static double trianglePerimeter;
    private static Shape circle;
    private static double circleArea;
    private static double circlePerimeter;
    private static Shape rectangle;
    private static double rectangleArea;
    private static double rectanglePerimeter;
    private static Collection<Shape> shapes;
    private static Map<String, Shape> shapeMap;

    private static final double DELTA = Math.pow(10, -8);

    @BeforeClass
    public static void setUp() throws Exception {
        double a = 3, b = 4, c = 5;
        triangle = new Triangle(a, b, c);
        trianglePerimeter = a + b + c;
        double s = trianglePerimeter / 2;
        triangleArea = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        double radius = 1;
        circle = new Circle(radius);
        circleArea = Math.PI * Math.pow(radius, 2);
        circlePerimeter = 2 * Math.PI * radius;
        double width = 2, heigh = 5;
        rectangle = new Rectangle(width, heigh);
        rectangleArea = width * heigh;
        rectanglePerimeter = 2 * (width + heigh);

        shapes = new ArrayList<>();
        shapes.add(triangle);
        shapes.add(circle);
        shapes.add(rectangle);

        shapeMap = new HashMap<>();
        shapeMap.put("Triangle", triangle);
        shapeMap.put("Circle", circle);
        shapeMap.put("Rectangle", rectangle);
    }

    @Test
    public void testTriangleArea() {
        assertEquals(triangleArea, triangle.getArea(), DELTA);
    }

    @Test
    public void testCircleArea() {
        assertEquals(circleArea, circle.getArea(), DELTA);
    }

    @Test
    public void testRectangleArea() {
        assertEquals(rectangleArea, rectangle.getArea(), DELTA);
    }

    @Test
    public void testTrianglePerimeter() {
        assertEquals(trianglePerimeter, triangle.getPerimeter(), DELTA);
    }

    @Test
    public void testCirclePerimeter() {
        assertEquals(circlePerimeter, circle.getPerimeter(), DELTA);
    }

    @Test
    public void testRectanglePerimeter() {
        assertEquals(rectanglePerimeter, rectangle.getPerimeter(), DELTA);
    }

    @Test
    public void testCollectionArea() {
        double expectedArea = triangleArea + circleArea + rectangleArea;
        double actualArea = shapes.stream().mapToDouble(Shape::getArea).sum();

        assertEquals(expectedArea, actualArea, DELTA);
    }

    @Test
    public void testCollectionPerimeter() {
        double expectedAveragePerimeter = (trianglePerimeter + circlePerimeter + rectanglePerimeter) / 3;
        double actualAveragePerimeter = shapes.stream().mapToDouble(Shape::getPerimeter).average().orElse(0);

        assertEquals(expectedAveragePerimeter, actualAveragePerimeter, DELTA);
    }

    @Test
    public void testModifiedCollectionArea() {
        double radius = 0.5;
        Shape circle = new Circle(radius);
        shapes.add(circle);
        double expectedArea = triangleArea + circleArea + rectangleArea + Math.PI * Math.pow(radius, 2);
        double actualArea = shapes.stream().mapToDouble(Shape::getArea).sum();
        shapes.remove(circle);

        assertEquals(expectedArea, actualArea, DELTA);
    }

    @Test
    public void testModifiedCollectionPerimeter() {
        double radius = 0.5;
        Shape circle = new Circle(radius);
        shapes.add(circle);
        double expectedAveragePerimeter = (trianglePerimeter + circlePerimeter + rectanglePerimeter + 2 * Math.PI * radius) / 4;
        double actualAveragePerimeter = shapes.stream().mapToDouble(Shape::getPerimeter).average().orElse(0);
        shapes.remove(circle);

        assertEquals(expectedAveragePerimeter, actualAveragePerimeter, DELTA);
    }

    @Test
    public void testMap() {
        double expectedArea = triangleArea + circleArea + rectangleArea;
        double actualArea = 0;
        double expectedAveragePerimeter = (trianglePerimeter + circlePerimeter + rectanglePerimeter) / 3;
        double actualAveragePerimeter = 0;
        for (Map.Entry<String, Shape> shapeEntry : shapeMap.entrySet()) {
            actualArea += shapeEntry.getValue().getArea();
            actualAveragePerimeter += shapeEntry.getValue().getPerimeter();
        }
        actualAveragePerimeter/= shapeMap.size();

        assertEquals(expectedArea, actualArea, DELTA);
        assertEquals(expectedAveragePerimeter, actualAveragePerimeter, DELTA);
    }
}
