package tk.sadbuttrue.parser.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Eugene Aslanov
 */
public class StringProcessorTest {
    @Test
    public void processAdd() {
        Integer expected = 4;
        Integer actual = StringProcessor.process("1+3");

        assertEquals(expected, actual);
    }

    @Test
    public void processSub() {
        Integer expected = 4;
        Integer actual = StringProcessor.process("9-5");

        assertEquals(expected, actual);
    }

    @Test
    public void processMul() {
        Integer expected = 16;
        Integer actual = StringProcessor.process("8*2");

        assertEquals(expected, actual);
    }

    @Test
    public void processDiv() {
        Integer expected = 16;
        Integer actual = StringProcessor.process("64/4");

        assertEquals(expected, actual);
    }

    @Test
    public void processWithPriority() {
        Integer expected = 5;
        Integer actual = StringProcessor.process("10-50/10");

        assertEquals(expected, actual);
    }

    @Test
    public void processWithBraces() {
        Integer expected = 1;
        Integer actual = StringProcessor.process("5 - (1 + 3)");

        assertEquals(expected, actual);
    }

    @Test
    public void processWithPriorityAndBraces() {
        Integer expected = -21;
        Integer actual = StringProcessor.process("(10 * 2 +4) - 15 * 3");

        assertEquals(expected, actual);
    }
}
