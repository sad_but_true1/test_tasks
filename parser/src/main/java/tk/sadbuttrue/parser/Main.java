package tk.sadbuttrue.parser;

import tk.sadbuttrue.parser.util.StringProcessor;

import java.util.Scanner;

/**
 * @author Eugene Aslanov
 */
public class Main {
    /**
     * @param args expects full expression in the first string (args[0]) or every part of expression in separate string or nothing
     */
    public static void main(String args[]) {
        String expression;
        Integer result;
        if (args.length != 0) {
            if (args.length == 1) {
                expression = args[0];
            } else {
                StringBuilder strBuilder = new StringBuilder();
                for (String arg : args) {
                    strBuilder.append(arg);
                }
                expression = strBuilder.toString();
            }
        } else {
//            If we don't have any console args we'll ask user to input expression right in program
            System.out.println("Enter an expression:");
            Scanner in = new Scanner(System.in);
            expression = in.nextLine();
            in.close();
        }
        result = StringProcessor.process(expression);
        System.out.println("Result: " + result);
    }
}
