package tk.sadbuttrue.parser.model;

/**
 * Describes type of operation
 * @author Eugene Aslanov
 */
public enum Operation {
    ADD, SUB, MUL, DIV;

    /**
     * Convert character to {@link Operation}
     * @param operation character that we want to convert
     * @return corresponding {@link Operation}
     */
    public static Operation getOp(char operation) {
        switch (operation) {
            case '+':
                return Operation.ADD;

            case '-':
                return Operation.SUB;

            case '*':
                return Operation.MUL;

            case '/':
                return Operation.DIV;

            default:
                throw new RuntimeException("No such operation");
        }
    }
}
