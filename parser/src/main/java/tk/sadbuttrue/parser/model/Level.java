package tk.sadbuttrue.parser.model;

/**
 * Concrete {@link Element} that describes level
 * @author Eugene Aslanov
 */
public class Level extends Element<Integer> {
    /**
     * Constructs {@link Level} from value
     * @param value value of new {@link Level}
     */
    public Level(Integer value) {
        super(value);
    }

    /**
     * Add income value to current level
     * @param level level values change
     */
    public void addLevel(Integer level) {
        this.value += level;
    }
}
