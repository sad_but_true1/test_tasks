package tk.sadbuttrue.parser.model;

/**
 * Describes element of expression
 * @author Eugene Aslanov
 */
public abstract class Element<T> {
    protected T value;

    /**
     * Getter for value of current element
     * @return value
     */
    public T getVal() {
        return value;
    }

    /**
     * Constructs {@link Element} from it's value
     * @param value value of new element
     */
    public Element(T value) {
        this.value = value;
    }
}
