package tk.sadbuttrue.parser.model;

/**
 * Concrete {@link Element} that describes operation
 * @author Eugene Aslanov
 */
public class Operator extends Element<Operation> {
    /**
     * Constructs {@link Operator} from it's value
     * @param value new {@link Operation} value of {@link Operator}
     */
    public Operator(Operation value) {
        super(value);
    }
}
