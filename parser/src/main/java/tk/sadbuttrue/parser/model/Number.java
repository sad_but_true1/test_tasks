package tk.sadbuttrue.parser.model;

/**
 * Concrete {@link Element} that describes decimal number
 * @author Eugene Aslanov
 */
public class Number extends Element<Integer> {
    /**
     * Constructs {@link Number} from value
     * @param value value of new {@link Number}
     */
    public Number(Integer value) {
        super(value);
    }
}
