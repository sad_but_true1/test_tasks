package tk.sadbuttrue.parser.util;

import tk.sadbuttrue.parser.model.*;
import tk.sadbuttrue.parser.model.Number;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Eugene Aslanov
 */
public class StringProcessor {
    public static Integer process(String inputString) {
        String string = inputString.replace(" ", "");
//        LinkedList because it implements both Deque and List
        LinkedList<Element> elements = new LinkedList<>();

//        open brackets
        pushLevel(elements, 3);
        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            Character c = chars[i];
            switch (c) {
                case '(':
//                    open brackets
                    pushLevel(elements, 3);
                    break;
                case ')':
//                    close brackets
                    popLevel(elements, 3);
                    break;
                case '*':
                case '/':
//                    decrease priority of previous brackets
                    popLevel(elements, 1);
//                    push opertor
                    pushOperator(elements, c);
//                    open next low-priority brackets
                    pushLevel(elements, 1);
                    break;
                case '+':
                case '-':
                    if (i == 0 || "(+/-*".contains(String.valueOf(chars[i - 1]))) {
//                        in the beginning or on previous position we have operation we simply push operator
                        pushOperator(elements, c);
                    } else {
//                        in any other cases we try to make prev brackets lowest, push operator and add mid-prior bracket
                        popLevel(elements, 2);
                        pushOperator(elements, c);
                        pushLevel(elements, 2);
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
//                    build number and push it to expression
                    int current = 0;
                    while (i < chars.length) {
                        current = 10 * current + Integer.parseInt(c.toString());
                        if (i + 1 == chars.length || !Character.isDigit(chars[i + 1])) {
                            break;
                        }
                        i++;
                        c = chars[i];
                    }
                    pushNumber(elements, current);
                    break;
                default:
                    throw new RuntimeException("Unexpected symbol");
            }
        }
//        close first bracket
        popLevel(elements, 3);

        if (elements.size() == 0 || elements.size() > 1) {
            throw new RuntimeException("Incomplete expression");
        }

//        in the end we have only one number and return it's value
        return ((Number) elements.getLast()).getVal();
    }

    /**
     * Push to end desired level or increase existing level
     * @param elements {@link Deque} of elements of expression
     * @param level desired level
     */
    private static void pushLevel(Deque<Element> elements, Integer level) {
        if (elements.isEmpty() || !(elements.getLast() instanceof Level)) {
            elements.addLast(new Level(level));
        } else {
            ((Level) elements.getLast()).addLevel(level);
        }
    }

    /**
     * Push to end operator
     * @param elements {@link Deque} of elements of expression
     * @param operationChar character representation of operation
     */
    private static void pushOperator(Deque<Element> elements, char operationChar) {
        Operation operation = Operation.getOp(operationChar);
        Operator result = new Operator(operation);
        elements.addLast(result);
    }

    /**
     * Push to end number
     * @param elements {@link Deque} of elements of expression
     * @param number desired value of number
     * @param <Delist> using inheritor of {@link Deque} and {@link List} for pushing and popping ({@link Deque}) and for getting by index ({@link List})
     */
    private static <Delist extends Deque<Element> & List<Element>> void pushNumber(Delist elements, Integer number) {
        if (elements.isEmpty() || elements.getLast() instanceof Level ||
                (elements.getLast() instanceof Operator && elements.size() > 1 && elements.get(elements.size() - 2) instanceof Number)) {
//            if we have nothing or level on the end or number and operator on the end we simply push number to the end
            elements.addLast(new Number(number));
        } else if (elements.getLast() instanceof Operator && (elements.size() == 1 || elements.get(elements.size() - 2) instanceof Level)) {
//            if we have unary operator or level and operator we need more complex logic
            if (((Operator) elements.getLast()).getVal().equals(Operation.MUL) || ((Operator) elements.getLast()).getVal().equals(Operation.DIV)) {
//                no multiplication or division allowed here
                throw new RuntimeException("Unexpected operation (* or /)");
            }
            if (((Operator) elements.getLast()).getVal().equals(Operation.SUB)) {
//                inversion for unary minus
                number *= -1;
            }
//            replace last to new number
            elements.removeLast();
            elements.addLast(new Number(number));
        } else {
            throw new RuntimeException("Unexpected number");
        }
    }

    /**
     * Decrease level
     * @param elements current elements
     * @param level desirable level
     * @param <Delist> using inheritor of {@link Deque} and {@link List} for pushing and popping ({@link Deque}) and for getting by index ({@link List})
     */
    private static <Delist extends Deque<Element> & List<Element>> void popLevel(@Nonnull Delist elements, @Nonnull Integer level) {
        if (level == 0) {
//            If we lowest level and enough elements we evaluate them and add result to the tail
            if (elements.size() > 3) {
                elements.addLast(calculateOperation(elements));
            }
//            Leave method if we have nothing to evaluate
            return;
        }
        if (elements.isEmpty() || elements.getLast() instanceof Level || elements.getLast() instanceof Operator) {
//            we can not close brackets when expression is empty or level in the end or operator in the end (same as we need non-empty expression with number in the end)
            throw new RuntimeException("Unexpected )");
        }

        int len = elements.size();
        if (len > 1 && elements.get(len - 2) instanceof Level) {
//            in case we have open brackets before last element
            if (((Level) elements.get(len - 2)).getVal() > level) {
//                when level of prior brackets is greater we decrease it on desired level
                ((Level) elements.get(len - 2)).addLevel(-level);
            } else {
//                if lower we replace current level number to number and try pop again with distance between levels
                int delta = level - ((Level) elements.get(len - 2)).getVal();
                Element last = elements.getLast();
                elements.removeLast();
                elements.removeLast();
                elements.addLast(last);
                popLevel(elements, delta);
            }
        } else if (len > 3) {
//            try to evaluate last 3 elements
            Element number = calculateOperation(elements);
            if (elements.getLast() instanceof Level) {
//            same as previous but with adding evaluated number to the end
                if (((Level) elements.getLast()).getVal() > level) {
                    ((Level) elements.getLast()).addLevel(-level);
                    elements.addLast(number);
                } else {
                    int delta = level - ((Level) elements.getLast()).getVal();
                    elements.removeLast();
                    elements.addLast(number);
                    popLevel(elements, delta);
                }
            } else if (elements.getLast() instanceof Operator) {
//                for operator in the end we add number and pop again
                elements.addLast(number);
                popLevel(elements, level);
            } else {
                throw new RuntimeException("Unexpected )");
            }
        } else {
            throw new RuntimeException("Unexpected )");
        }
    }

    /**
     * Evaluate expression (number operator number)
     * @param elements {@link Deque} of three or more elements
     * @return {@link Number} with result of evaluation
     */
    private static Element calculateOperation(Deque<Element> elements) {
        if (elements.size() <= 3) {
            throw new IllegalArgumentException("Expected exact three elements");
        }
        Element numberRight = elements.getLast();
        elements.removeLast();
        Element operation = elements.getLast();
        elements.removeLast();
        Element numberLeft = elements.getLast();
        elements.removeLast();

        if (!(numberLeft instanceof Number) || !(numberRight instanceof Number) || !(operation instanceof Operator)) {
            throw new RuntimeException("Unexpected )");
        }

        Integer result = ((Number) numberLeft).getVal();
        Integer nRight = ((Number) numberRight).getVal();
        switch (((Operator) operation).getVal()) {
            case ADD:
                result += nRight;
                break;
            case SUB:
                result -= nRight;
                break;
            case MUL:
                result *= nRight;
                break;
            case DIV:
                result /= nRight;
                break;
        }
        return new Number(result);
    }
}
