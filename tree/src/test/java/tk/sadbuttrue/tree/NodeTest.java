package tk.sadbuttrue.tree;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Eugene Aslanov
 */
public class NodeTest {
    private static Node<Double> node;
    private static Node<Double> equalNode;
    private static Node<Double> unequalNode;

    @BeforeClass
    public static void setUp() throws Exception {
        node = new Node<Double>(null, 10.0);
        node.addChild(new Node<Double>(node, 11.0));
        node.addChild(new Node<Double>(node, 12.0));

        equalNode = new Node<Double>(null, 10.0);
        equalNode.addChild(new Node<Double>(equalNode, 11.0));
        equalNode.addChild(new Node<Double>(equalNode, 12.0));

        unequalNode = new Node<Double>(null, 10.0);
        unequalNode.addChild(new Node<Double>(unequalNode, 11.0));
        unequalNode.addChild(new Node<Double>(unequalNode, 12.0));
        Node<Double> unequalChild = new Node<Double>(unequalNode, 114.0);
        unequalChild.addChild(new Node<Double>(unequalChild, 13.0));
        unequalNode.addChild(unequalChild);
    }

    @Test
    public void selfEquals() throws Exception {
        assertTrue(node.equals(node));
    }

    @Test
    public void equals() throws Exception {
        assertTrue(node.equals(equalNode));
    }

    @Test
    public void equalHashCodes() throws Exception {
        assertEquals(node.hashCode(), equalNode.hashCode());
    }

    @Test
    public void unequals() throws Exception {
        assertFalse(node.equals(unequalNode));
    }

    @Test
    public void unequalHashCodes() throws Exception {
        assertFalse(node.hashCode() == unequalNode.hashCode());
    }

}