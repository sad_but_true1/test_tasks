package tk.sadbuttrue.tree;

import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Describes node of tree
 * @author Eugene Aslanov
 */
public class Node<T> {
    private boolean isRoot;

    private Node<T> parent;

    @Nonnull
    private T value;

    @Nonnull
    private Collection<Node<T>> children;

    /**
     * Constructs node from full set of params
     * @param isRoot – is node root or not (parent should be null if node is root)
     * @param parent – link to parent node (node shouldn't be root if parent isn't null)
     * @param value – value that stored in this node
     * @param children – {@link Collection} of children (if empty, you should use instance of {@link ArrayList} or other implementation instead {@link java.util.Collections#emptyList}
     */
    public Node(boolean isRoot, Node<T> parent, @Nonnull T value, @Nonnull Collection<Node<T>> children) {
        if ((isRoot && parent != null) || (!isRoot && parent == null)) {
            throw new IllegalArgumentException("Root node shouldn't have parent and node with parent shouldn't be root");
        }
        this.isRoot = isRoot;
        this.parent = parent;
        this.value = value;
        this.children = children;
    }

    /**
     * Constructs node from
     * @param parent – link to parent node (if parent is null we think that node is root)
     * @param value – value that stored in this node
     * @param children – {@link Collection} of children (if empty, you should use instance of ArrayList or other implementation instead Collections.emptyList
     */
    public Node(Node<T> parent, @Nonnull T value, @Nonnull Collection<Node<T>> children) {
        this(parent == null, parent, value, children);
    }

    /**
     * Constructs node with empty collection of children from
     * @param parent – link to parent node (if parent is null we think that node is root)
     * @param value – value that stored in this node
     */
    public Node(Node<T> parent, @Nonnull T value) {
//        instance of ArrayList instead Collections.emptyList because Collections.emptyList immutable (we want add children)
        this(parent == null, parent, value, new ArrayList<>());
    }

    /**
     * Getter for root
     * @return boolean root value
     */
    public boolean isRoot() {
        return isRoot;
    }

    /**
     * Setter for root. Also changes link to parent
     * @param root new value of root
     */
    public void setRoot(boolean root) {
        isRoot = root;
//        if we become root, we can not have parent
        if (root && parent != null) {
            parent = null;
        }
    }

    /**
     * Getter for parent
     * @return Node that is parent for current
     */
    public Node<T> getParent() {
        return parent;
    }

    /**
     * Setter for parent. Also changes root
     * @param parent new parent value
     */
    public void setParent(Node<T> parent) {
        this.parent = parent;
//        if we haven't parent, we become root
        if (parent == null) {
            isRoot = true;
        }
    }

    /**
     * Getter for current node value
     * @return value of node
     */
    public T getValue() {
        return value;
    }

    /**
     * Setter for node value
     * @param value new node value
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Getter for children
     * @return {@link Node}
     */
    public Collection<Node<T>> getChildren() {
        return children;
    }

    /**
     * Setter for children
     * @param children new children {@link Collection} of {@link Node}
     */
    public void setChildren(Collection<Node<T>> children) {
        this.children = children;
    }

    /**
     * Append new child to node
     * @param child new node child
     */
    public void addChild(Node<T> child) {
        children.add(child);
    }

    /**
     * Checks equality of subtrees (we don't count isRoot and parent because if we'll count it only self-equality will be true)
     * @param o other {@link Node}
     * @return boolean value of equality
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node<?> node = (Node<?>) o;

        if (!value.equals(node.value)) return false;
        if (children.size() != node.children.size()) return false;
        if (children.isEmpty() && node.children.isEmpty()) return true;
//        We want to ensure that children not only same, but in same order
//        For that we use fullscan and also checks that we not already test current value through thatEqualChildren collection
        Collection<Node<?>> thatEqualChildren = new ArrayList<>();
        for (Node<?> thisChild : children) {
            node.children.stream().filter(thatChild -> !thatEqualChildren.contains(thatChild)).forEach(thatChild -> {
                boolean comparison = thisChild.equals(thatChild);
                if (comparison) {
                    thatEqualChildren.add(thatChild);
                }
            });
        }
//        Checks that all children from that was equal to children from this
        for (Node<?> thatChild : node.children) {
            if (!thatEqualChildren.contains(thatChild)) return false;
        }
        return true;
//        Alternative way is to use Apache commons-collections
//        return CollectionUtils.isEqualCollection(children, node.children);
    }

    /**
     * We also don't count isRoot and parent (for match {@link Node#equals(Object)})
     * @return int value of hash code for object
     */
    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + children.hashCode();
        return result;
    }
}
